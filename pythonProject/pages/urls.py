# from django.urls import path, include

# from .views import *
# urlpatterns = [
#     path('', HomeView.as_view(), name="home"),
#     path('post/<int:pk>/', PostDetailView.as_view(), name="post_detail")
# ]

from django.urls import path, include

from .views import *
urlpatterns = [
    path('', home, name="home"),
    path('post/<int:pk>/', post_detail, name="post_detail"),
    path('views.featured_post', featured_posts, name='featured_post'),
]