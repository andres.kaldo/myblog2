from django.db import models
from django.contrib.auth.models import User


class Category(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name


class Posts(models.Model):
    title = models.CharField(max_length=100)
    author = models.CharField(max_length=100)
    content = models.TextField()
    date_posted = models.DateField()
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='posts',null=True,blank=True)

    def __str__(self):
        return f"{self.title}-{self.author}"


class Comment(models.Model):
    post = models.ForeignKey(Posts, on_delete=models.CASCADE, related_name='comments')
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    content = models.TextField()
    date_posted = models.DateField(auto_now_add=True)

    def __str__(self):
        return f"Comment by {self.author.username} on {self.post.title}"


class FeaturedPost(models.Model):
    post = models.OneToOneField(Posts, on_delete=models.CASCADE)
    priority = models.IntegerField(default=0)
    start_date = models.DateField()
    end_date = models.DateField()

    def __str__(self):
        return f"Featured Post: {self.post.title}"

# pealelõunane ülesanne

class Featured_post(models.Model):
    title = models.CharField(max_length=200)
    content = models.TextField()
    is_featured = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)





