from django.contrib import admin
from .models import Posts, Category, Comment, FeaturedPost
from django.contrib.auth.models import User
# Register your models here.

admin.site.register(Posts)
admin.site.register(Category)
admin.site.register(Comment)
admin.site.register(FeaturedPost)
# admin.site.register(Featured_post)
