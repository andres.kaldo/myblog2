# from django.shortcuts import render, redirect, get_object_or_404
# from django.views.generic import ListView, DetailView
# from .form import CommentForm
#
#
# from .models import Posts
# # functional Based views
# def home(request):
#     # Sample blog data
#     posts = Posts.objects.all()
#     posts = Posts.objects.order_by('-date_posted')
#     context = {'posts': posts}
#     return render(request, 'home.html', context)
#
#
# # class based Views
# class HomeView(ListView):
#     model = Posts
#     template_name = 'home.html'
#     context_object_name = 'posts'
#     ordering = ['title']
#
#
# class PostDetailView(DetailView):
#     model = Posts
#     template_name = 'posts/post_detail.html'
#
#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         context['comments'] = self.object.comments.all()
#         return context
#
# def post_detail(request, pk):
#     # Retrieve the Post object
#     post = get_object_or_404(Posts, pk=pk)
#
#     # Handle the POST request
#     if request.method == 'POST':
#         comment_form = CommentForm(request.POST)
#         if comment_form.is_valid():
#             comment = comment_form.save(commit=False)
#             comment.author = request.user
#             comment.post = post
#             comment.save()
#             return redirect('post_detail', pk=post.pk)
#     else:
#         comment_form = CommentForm()
#
#     # Context data for rendering the template
#     context = {
#         'post': post,
#         'comments': post.comments.all(),
#         'comment_form': comment_form
#     }
#
#     return render(request, 'posts/post_detail.html', context)

from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import ListView , DetailView
from .form import CommentForm
from .models import FeaturedPost

import pdb

from .models import Posts
# functional Based views
def home(request):
    # Sample blog data
    posts = Posts.objects.all()
    posts = Posts.objects.order_by('-date_posted')
    featured_posts = FeaturedPost.objects.all().order_by('priority')
    context = {'posts': posts, 'featured_posts':featured_posts}
    return render(request, 'home.html', context)

class HomeView(ListView):
    model = Posts
    template_name = 'home.html'
    context_object_name = 'posts'
    ordering = ['title']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['featured_posts'] = FeaturedPost.objects.all().order_by('priority')
        return context


#class based Views
class HomeView(ListView):
    model = Posts
    template_name = 'home.html'
    context_object_name = 'posts'
    ordering = ['title']


# class PostDetailView(DetailView):
#     model = Posts
#     template_name = 'posts/post_detail.html'
#
#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         context['comments'] = self.object.comments.all()
#         return context
#
#     def post(self,request,*args,**kwargs):
#         pdb.set_trace()
#         print(request.POST)
#         self.object = self.get_object()
#         comment_form = CommentForm(request.POST)
#         if comment_form.is_valid():
#             comment = comment_form.save(commit=False)
#             comment.author = request.user
#             comment.post = self.object
#             comment.save()
#             return redirect('post_detail', pk=self.object.id)
#         context = self.get_context_data(object=self.object)
#         context['comment_form'] = comment_form
#         return self.render_to_response(context)

def post_detail(request, pk):
    # Retrieve the Post object
    post = get_object_or_404(Posts, pk=pk)

    # Handle the POST request
    if request.method == 'POST':
        comment_form = CommentForm(request.POST)
        if comment_form.is_valid():
            comment = comment_form.save(commit=False)
            comment.author = request.user
            comment.post = post
            comment.save()
            return redirect('post_detail', pk=post.pk)
    else:
        comment_form = CommentForm()

    # Context data for rendering the template
    context = {
        'post': post,
        'comments': post.comments.all(),
        'comment_form': comment_form
    }

    return render(request, 'posts/post_detail.html', context)

# pealelõunane ülesanne

def featured_posts(request):
    featured_posts = Posts.objects.filter(is_featured=True).order_by('-created_at')
    context = {
        'featured_posts': featured_posts
    }
    return render(request, 'home.html', context)