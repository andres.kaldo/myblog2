from django.shortcuts import render


def contact(request):
    return render(request, 'contact.html')

def about(request):
    return render(request, 'about.html')

def link(request):
    return render(request, 'link.html')
