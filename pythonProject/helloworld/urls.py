from django.contrib import admin
from django.urls import path,include
from .views import *

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('pages.urls')),
    path('contact/',  contact,  name='contact'),
    path('about/',  about,  name='about'),
    path('link/',  link,  name='link'),
]
